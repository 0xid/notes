package com.example.notes;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class DataModel {
    private ArrayList<Note> notes;
    private DBHelper dbhelper;

    DataModel(Context context) {
        dbhelper = new DBHelper(context);
        notes = dbhelper.LoadNotes();
    }

    DataModel(Context context, ArrayList<Note> _notes) {
        dbhelper = new DBHelper(context);
        notes = _notes;
    }

    public ArrayList<Note> GetNotes() {
        return notes;
    }

    public void InsertNote(String title, String text) {
        int id = dbhelper.InsertNote(title, text);
        if (id >= 0)
            notes.add(new Note(id, title, text));
    }


    public void ChangeNote(int id, String title, String text) {
        for (Note n: notes ) {
            if (n.GetId() == id) {
                n.Change(title, text);
                break;
            }
        }

        dbhelper.UpdateNote(id, title, text);
    }

    public void DeleteNote(int id)
    {
        Note toDelete = null;
        for (Note n: notes) {
            if (n.GetId() == id) {
                toDelete = n;
                break;
            }
        }
        if (toDelete != null)
            notes.remove(toDelete);

        dbhelper.DeleteNote(id);
    }


    public Note GetNoteByPosition(int position) {
        return notes.get(position);
    }

    public void Destroy() {
        dbhelper.Close();
    }

}
