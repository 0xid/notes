package com.example.notes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "NotesDB";
    private static final int DB_VERSION = 1;

    private SQLiteDatabase db;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String cmd = "CREATE TABLE NOTES ("
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "Title TEXT, "
                + "Text TEXT);";
        sqLiteDatabase.execSQL(cmd);
        db = sqLiteDatabase;
        InsertNote("Title 1", "Sample text 1");
        InsertNote("Title 2", "Sample text 2");
        InsertNote("Title 3", "Sample text 3");
    }

    public int InsertNote(String title, String text) {
        if (db == null) db = getWritableDatabase();
        ContentValues noteValues = new ContentValues();
        noteValues.put("Title", title);
        noteValues.put("Text", text);
        return (int) db.insert("NOTES", null, noteValues);
    }

    public void UpdateNote(int id, String newTitle, String newText) {
        if (db == null) db = getWritableDatabase();
        ContentValues noteValues = new ContentValues();
        noteValues.put("Title", newTitle);
        noteValues.put("Text", newText);
        db.update("NOTES", noteValues, "_id = ?", new String[] {Integer.toString(id)});
    }

    public ArrayList<Note> LoadNotes() {
        if (db == null) db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM NOTES", null);
        ArrayList<Note> notes = new ArrayList<>();
        while (cursor.moveToNext())
        {
            Note note = new Note(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
            notes.add(note);
        }
        cursor.close();
        return notes;
    }


    public void DeleteNote(int id) {
        if (db == null) db = getWritableDatabase();
        db.delete("NOTES", "_id = ?", new String[] {Integer.toString(id)});
    }

    public void Close() {
        if (db != null) db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
