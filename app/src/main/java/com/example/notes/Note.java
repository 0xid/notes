package com.example.notes;


import android.os.Parcel;
import android.os.Parcelable;

public class Note implements Parcelable {
    private int id;
    private String title;
    private String text;

    Note() {
        id = 0;
        title = "";
        text = "";
    }

    Note(int _id, String _title, String _text) {
        id = _id;
        title = _title;
        text = _text;
    }

    protected Note(Parcel in) {
        id = in.readInt();
        title = in.readString();
        text = in.readString();
    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(title);
        parcel.writeString(text);
    }

    public int GetId() { return id; }

    public String GetTitle() {
        return  title;
    }

    public String GetText() {
        return  text;
    }

    public void  Change(String _title, String _text) {
        title = _title;
        text = _text;
    }
}
