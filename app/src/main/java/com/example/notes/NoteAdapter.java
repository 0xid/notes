package com.example.notes;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.ViewHolder> {
    private ArrayList<Note> data;
    private OnNoteClickListener listener;

    public NoteAdapter(ArrayList<Note> ns, OnNoteClickListener listener) {
        data = ns;
        this.listener = listener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_note, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(data.get(position), listener);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView text;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.card_note_title);
            text = (TextView) itemView.findViewById(R.id.card_note_text);
        }

        public void bind(final Note note, final OnNoteClickListener listener) {
            title.setText(note.GetTitle());
            text.setText(note.GetText());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onNoteClick(note);
                }
            });
        }
    }

    public interface OnNoteClickListener {
        void onNoteClick(Note note);
    }
}
