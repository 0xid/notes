package com.example.notes;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;

public class NotesViewActivity extends AppCompatActivity implements NoteAdapter.OnNoteClickListener{

    private NoteAdapter adapter;
    private DataModel model;
    private final int REQUEST_CODE_EDIT = 1;
    private final int REQUEST_CODE_NEW = 2;
    private final int GRID_COLUMNS_PORTRAIT = 2;
    private final int GRID_COLUMNS_LANDSCAPE = 3;
    private final static String STATE_NOTES = "notes";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_view);
        RecyclerView rv = (RecyclerView) findViewById(R.id.note_view_grid);
        GridLayoutManager glm = new GridLayoutManager(this,
                getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ?
                    GRID_COLUMNS_PORTRAIT : GRID_COLUMNS_LANDSCAPE);
        rv.setLayoutManager(glm);



        if (savedInstanceState !=  null) {
            ArrayList<Note> notes = savedInstanceState.getParcelableArrayList(STATE_NOTES);
            model = new DataModel(this, notes);
        }
        else {
            model = new DataModel(this);
        }

        adapter = new NoteAdapter(model.GetNotes(), this);
        rv.setAdapter(adapter);
    }


    @Override
    public void onNoteClick(Note note) {
        Intent editNote = new Intent(NotesViewActivity.this, NoteEditActivity.class);
        editNote.putExtra("title", note.GetTitle());
        editNote.putExtra("text", note.GetText());
        editNote.putExtra("isNew", false);
        editNote.putExtra("id", note.GetId());
        startActivityForResult(editNote, REQUEST_CODE_EDIT);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        model.Destroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        ArrayList<Note> notes = model.GetNotes();
        outState.putParcelableArrayList(STATE_NOTES, notes);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) return;
        if (requestCode == REQUEST_CODE_NEW && resultCode == RESULT_OK)
        {
            String newTitle = data.getStringExtra("title");
            String newText = data.getStringExtra("text");
            model.InsertNote(newTitle, newText);
            adapter.notifyDataSetChanged();
        }
        else if (requestCode == REQUEST_CODE_EDIT && resultCode == RESULT_OK)
        {
            String newTitle = data.getStringExtra("title");
            String newText = data.getStringExtra("text");
            int id = data.getIntExtra("id", 0);
            model.ChangeNote(id, newTitle, newText);
            adapter.notifyDataSetChanged();
        }
        else if (requestCode == REQUEST_CODE_EDIT && resultCode == RESULT_FIRST_USER)
        {
            int id = data.getIntExtra("id", 0);
            if (id != 0)
                model.DeleteNote(id);
            adapter.notifyDataSetChanged();
        }
    }

    public void onFloatButtonClick(View view) {
        Intent addNewNote = new Intent(this, NoteEditActivity.class);
        addNewNote.putExtra("isNew", true);
        startActivityForResult(addNewNote, REQUEST_CODE_NEW);
    }
}
