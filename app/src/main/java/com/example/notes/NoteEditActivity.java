package com.example.notes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class NoteEditActivity extends AppCompatActivity {

    private boolean isNew;
    private String oldTitle;
    private String oldText;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_edit);
        Intent intent = getIntent();
        isNew = intent.getBooleanExtra("isNew", false);
        if (!isNew) {
            oldTitle = intent.getStringExtra("title");
            oldText = intent.getStringExtra("text");
            id = intent.getIntExtra("id", 0);
            EditText contentView = (EditText)findViewById(R.id.note_edit_text);
            EditText titleView = (EditText)findViewById(R.id.note_edit_title);
            titleView.setText(oldTitle);
            contentView.setText(oldText);
        }
        else
            id = 0;
    }

    public void onEditSaveClick(View view) {
        Intent okIntent = new Intent();
        EditText contentView = (EditText)findViewById(R.id.note_edit_text);
        EditText titleView = (EditText)findViewById(R.id.note_edit_title);
        okIntent.putExtra("title", titleView.getText().toString());
        okIntent.putExtra("text", contentView.getText().toString());
        okIntent.putExtra("isNew", isNew);
        if (!isNew)
            okIntent.putExtra("id", id);
        setResult(RESULT_OK, okIntent);
        finish();
    }

    public void onEditDeleteClick(View view) {
        Intent delIntent = new Intent();
        delIntent.putExtra("id", id);
        setResult(RESULT_FIRST_USER, delIntent);
        finish();
    }

    public void onEditCancelClick(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

}
